# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Type(models.Model):
	type_name = models.CharField(max_length=200)

	def __unicode__(self):
		return self.type_name


class RegisterUsers(models.Model):
	type_name = models.ManyToManyField(Type, verbose_name='Выберите то что интересует вас:')
	phone_number = models.CharField(max_length=200, verbose_name='Введите номер телефона:')
	# activate = models.BooleanField(default=False, verbose_name='')

	def __unicode__(self):
		return self.phone_number


class Moderator(models.Model):
	news_name = models.CharField(max_length=200, verbose_name='Название происшествий')
	text = models.TextField(verbose_name='Основное сообщение')
	type_choice = models.ForeignKey(Type, verbose_name='Типы происшествий')
	unpublish = models.BooleanField(default=True, blank=True, verbose_name='Не публиковать')

	def __unicode__(self):
		return self.news_name




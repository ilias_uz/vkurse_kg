from django import forms
# from django.contrib.auth.models import User
from models import Moderator, RegisterUsers, Type


class ModeratorForm(forms.ModelForm):
    class Meta:
        model = Moderator
        fields = ('news_name', 'text', 'type_choice', 'unpublish')


class RegisterUsersForm(forms.ModelForm):
    class Meta:
        model = RegisterUsers
        fields = ('phone_number', 'type_name')
        widgets = {
            'type_name': forms.CheckboxSelectMultiple()
        }
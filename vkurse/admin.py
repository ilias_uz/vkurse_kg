# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Type, RegisterUsers, Moderator

# Register your models here.

admin.site.register(Type)
admin.site.register(RegisterUsers)
admin.site.register(Moderator)
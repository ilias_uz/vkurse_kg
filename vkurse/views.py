# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from forms import RegisterUsersForm, ModeratorForm
from models import Moderator, RegisterUsers, Type
from django.contrib.auth import authenticate, login as auth_login
from django.views.generic.edit import UpdateView
import requests
import random

from twilio.rest import Client
# Create your views here.

client = Client('ACb4ba5c496f86ee7b251678a7842a4bd6', '0ae926e0a08e8b5ffa32daa0f125a6d4')

def index(request):
	if request.method == 'POST':
		form = RegisterUsersForm(request.POST)
		phone_num = request.POST['phone_number']
		if form.is_valid():
			check_number = RegisterUsers.objects.all().filter(phone_number=request.POST['phone_number']).first()
			if not check_number:
				# ran_num = random.randrange(0, 101, 4)
				# request.session[phone_num] = ran_num
				# xml = '<?xml version="1.0" encoding="UTF-8"?>\
				# 		<message>\
				# 			<login>vkurse</login>\
				# 			<pwd>4J5cUmjm</pwd>\
				# 			<id>%s</id>\
				# 			<sender>996554012517</sender>\
				# 			<text>Код потвирждение: %s</text>\
				# 			<phones>\
				# 				<phone>%s</phone>\
				# 			</phones>\
				# 		</message>' % (ran_num, ran_num, request.POST['phone_number'])
				# headers = {'Content-Type': 'application/xml'}
				# r = requests.post('http://smspro.nikita.kg/api/message', data=xml, headers=headers)
				# return render(requests, 'confir_form.html', {'phone_num': request.POST['phone_number']})
				form.save()
				return render(request, 'thanks.html')
			else:
				phone_num = request.POST['phone_number']
				error = 'Номер %s уже был зарегистрирован! Пожалуйста выберите другой номер.' % phone_num
				form = RegisterUsersForm()
				return render(request, 'index.html', {'error': error, 'form': form})
	else:
		form = RegisterUsersForm()
		print form
	types = Type.objects.all()
	return render(request, 'index.html', {'form': form, 'types': types})

def moderator(request):
	if request.user.is_authenticated():
		moderator = Moderator.objects.all()
		return render(request, 'info_list.html', {'moderators': moderator})
	else:
		if request.method == 'POST':
		    username = request.POST.get('username')
		    password = request.POST.get('password')
		    user = authenticate(username=username, password=password)
		    if user is not None:
		        if user.is_active:
		            auth_login(request, user)
		            return redirect('/moderator_login')
		        else:
		            return redirect('/moderator_login')
		return render(request, 'login.html')

def add_info(request):
	if request.method == 'POST':
		form = ModeratorForm(request.POST)
		if form.is_valid():
			form.save()
			if 'unpublish' not in request.POST:
				type_id = request.POST['type_choice']
				types = Type.objects.get(id=type_id)
				users = RegisterUsers.objects.all().filter(type_name__id=type_id)
				for user in users:
					ran_num = random.randrange(0, 101, 2)
					body=u"Название"
					body=body.encode('ascii', 'ignore')
					xml = """<?xml version='1.0' encoding='UTF-8'?>
							<message>
								<login>nurishok</login>
								<pwd>WwmAfuHT</pwd>
								<id>%s</id>
								<sender>996700511147</sender>
								<text>%s</text>
								<phones>
									<phone>%s</phone>
								</phones>
							</message>""" % (ran_num, body, user.phone_number)
					headers = {'Content-Type': 'application/xml'}
					r = requests.post('http://smspro.nikita.kg/api/message', data=xml, headers=headers)
				return redirect('/moderator_login')
					# client.messages.create(
		   #                          to=user.phone_number,
		   #                          from_="+15627324560",
		   #                          body="Название: %s, Собщение: %s" % (request.POST['news_name'], request.POST['text']),
		   #                      )
	else:
		form = ModeratorForm()
	return render(request, 'add_info.html', {'form': form})


def infoUpdate(request, id):
	if request.user.is_authenticated():
		moderator = Moderator.objects.get(pk=id)
		form = ModeratorForm(request.POST or None, instance=moderator)
		if request.method == 'POST':
			if form.is_valid():
				form.save()
				if 'unpublish' not in request.POST:
					type_id = request.POST['type_choice']
					types = Type.objects.get(id=type_id)
					users = RegisterUsers.objects.all().filter(type_name__id=type_id)
					for user in users:
						ran_num = random.randrange(0, 101, 2)
						body=u"Название"
						body=body.encode('ascii', 'ignore')
						xml = """<?xml version='1.0' encoding='UTF-8'?>
								<message>
									<login>nurishok</login>
									<pwd>WwmAfuHT</pwd>
									<id>%s</id>
									<sender>996700511147</sender>
									<text>%s</text>
									<phones>
										<phone>%s</phone>
									</phones>
								</message>""" % (ran_num, body, user.phone_number)
						headers = {'Content-Type': 'application/xml'}
						r = requests.post('http://smspro.nikita.kg/api/message', data=xml, headers=headers)
						print r.text
				return redirect('/moderator_login')
	else:
		return redirect('/')
	return render(request, 'add_info.html', {'form': form})


def deleteInfo(request, id):
	if request.user.is_authenticated():
		moderator = Moderator.objects.get(pk=id).delete()
	else:
		return redirect('/')
	return redirect('/moderator_login')


# def check_num(request):
# 	if request.method == 'POST':
# 		print request.POST
# 	return redirect('/')
